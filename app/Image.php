<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    //relacion one to may / deuno a miuchos
    public function comments(){

        return $this->hasMany('App\Comment')->orderBy('id', 'desc');

    }

    //relacion one to my
    public function likes(){

        return $this->hasMany('App\Like');

    }

    // relacion de muchos a uno
    public function user(){

        return $this->belongsTo('App\User', 'user_id');

    }
}
